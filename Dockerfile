FROM ubuntu:22.04

ENV TZ=Etc/UTC LANG=en_US.UTF-8 LANGUAGE=en_US:en LC_ALL=en_US.UTF-8 DEBIAN_FRONTEND=noninteractive QT_SELECT=qt5
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -y update && \
    apt-get install -yq apt-transport-https locales software-properties-common curl && \
    curl -sL https://deb.nodesource.com/setup_16.x | bash - && \
    locale-gen en_US.UTF-8 && \
    apt-get -y update && \
    apt-get install -yq \
        python3 python-is-python3 \
        sudo \
        qtbase5-dev qt5-qmake 

ADD . /app/build_tools
WORKDIR /app/build_tools

ENV PYTHONPATH=/app/build_tools/scripts:/app/build_tools/scripts/core_common:/app/build_tools/scripts/core_common/modules:/app/build_tools/scripts/sdkjs_common:/app/build_tools/scripts/develop:/app/build_tools/scripts/develop/vendor

#RUN python3 ./tools/linux/automate.py
CMD bash