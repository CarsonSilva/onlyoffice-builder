#!/usr/bin/env python

import build_js
import config

config.parse()
build_js.make()
