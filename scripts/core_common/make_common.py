#!/usr/bin/env python

import glob

import boost
import cef
import curl
import glew
import googletest
import harfbuzz
import html2
import hunspell
import hyphen
import icu
import openssl
import v8
import websocket

import base
import config


def check_android_ndk_macos_arm(dir):
    if base.is_dir(dir + "/darwin-x86_64") and not base.is_dir(dir + "/darwin-arm64"):
        print("copy toolchain... [" + dir + "]")
        base.copy_dir(dir + "/darwin-x86_64", dir + "/darwin-arm64")
    return


def make():
    if (config.check_option("platform", "android")) and (base.host_platform() == "mac") and (base.is_os_arm()):
        for toolchain in glob.glob(base.get_env("ANDROID_NDK_ROOT") + "/toolchains/*"):
            if base.is_dir(toolchain):
                check_android_ndk_macos_arm(toolchain + "/prebuilt")

    boost.make()
    cef.make()
    icu.make()
    openssl.make()
    v8.make()
    html2.make()
    hunspell.make(False)
    harfbuzz.make()
    glew.make()
    hyphen.make()
    googletest.make()

    if config.check_option("module", "mobile"):
        if (config.check_option("platform", "android")):
            curl.make()
        websocket.make()
    return
