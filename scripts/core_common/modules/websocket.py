#!/usr/bin/env python

import base
import socket_io

config_file = base.get_script_dir() + "/../../core/Common/WebSocket/websocket.pri"


def make():
    # ixwebsocket.make()
    # socketrocket.make()
    socket_io.make()

    return
