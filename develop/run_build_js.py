#!/usr/bin/env python3

import sys

import base
import build_js
import config

base.cmd_in_dir(sys.argv[1] + '/build_tools/', 'python3', ['configure.py'])
config.parse()

build_js.build_js_develop(sys.argv[1])
